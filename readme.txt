------------------------------------
- Překlad
------------------------------------
Aplikace je překládána příkazem make.

Vyžadované knihovny:
 - OpenCV

Vyžadovaný překladač:
 - G++ (součást GCC)

------------------------------------
- Popis aplikace a její ovládání
------------------------------------

Vytvořený program je preložitelný a spustitelný na běžných Linuxových distribucích s knihovnou OpenCV. Testován byl na serveru merlin.fit.vutbr.cz a Fedora 29. Program je ovládán skrze CLI a využívá podmíněného překladu pro poskytování konfliktních funkcionalit (např. nelze zaznamenávat použité pixely během měření výkonnosti kvůli zkreslení výsledku). Je proto vytvořeno několik spustitelných souborů:

    hist_estimate}                  <-- základní verze, která provede měření přesnosti  
    show_hist_estimate}             <-- navíc vykreslí vybrané pixely
    perf_hist_estimate}             <-- provádí měření přesnosti a výkonnosti

Výsledky měření jsou dle režimu buď vypisovány na stdout nebo ukládány ve formátu CSV do složky "./csv". Ukládání do CSV probíhá, pokud nebyla zvolena konkrétní metoda (a jsou tedy spuštěny všechny). Výstupní soubory jsou pojmenovány dle jména vstupního obrázku.

Repozitář je dostupný na adrese: https://gitlab.com/hajdiktomas/zpo-projekt.

------------------------------------
- Argumenty CLI
------------------------------------

Aplikace přijímá řadu argumentů v dlouhém (--) a krátkém (-) formátu:

    file (-f) <cesta>               (vyžadováno)
    method (-m) <název>             (jedno z: M1-M8, P1-P3, P1V, P3V)
    iterations <integer>            (nastavuje počet iterací měření)
    visual (-v)                     (zobrazí vizuální reprezentaci histogramu a spočteného odhadu)
    print                           (vytiskne normalizované hodnoty histogramů na stdout)
    showPicture                     (zobrazí vstupní obrázek)
    help (-h)                       (vypíše nápovědu k použití)

    nth (-n) <integer>              (metody M1-M3)
    rand (-a) <double>              (metody M4-M7)
    sigma (-s) <double>             (metoda M7)
    clusters (-c) <integer>         (metoda M7 -- počet shluků)
    cache (-i) <integer>            (metody P1 a P1V -- velikost kroku)
    cacheMin (-j) <integer>         (metody P2-P4 a P3V -- minimální velikost kroku)
    cacheMax (-k) <integer>         (metody P2-P4 a P3V -- maximální velikost kroku)
    pixel (-p) <integer>            (metody P1-P3 -- velikost kroku pixelů v rámci cache-line)
    pixelMin (-q) <integer>         (metody P1V a P3V -- minimální velikost kroku pixelů v rámci cache-line)
    pixelMax (-r) <integer>         (metody P1V a P3V -- maximální velikost kroku pixelů v rámci cache-line)
    thresholdPixel (-t) <integer>   (metoda P4)
    thresholdCache (-u) <integer>   (metoda P4)


------------------------------------
- Návod k použití
------------------------------------

Pro spuštění konkrétní metody (např. M1) je použit následující příkaz:

>>>    ./hist_estimate -f <cesta> -m M1

Na stdout je následně vypsána přesnost metody (menší číslo je lepší) a počet pixelů použitý při odhadu. V tomto tvaru je použito výchozí nastavení parametrů metody (definováno v settings.h). Nastavení je možné změnit přidáním argumentů:

>>>   ./hist_estimate -f <cesta> -m M1 -n 14

Takto je spuštěna metoda výběru každého 14. pixelu obrázku.

Pokud není zvolena metoda, bude provedeno měření všech metod v cyklu s přednastavenými parametry (obecně snižující počet použitých pixelů od 1/10 až po 1/1000). Počty použitých pixelů jsou vypisovány na stdout, přesnost je zapsána do CSV souboru se jménem "<název_obrázku>.accur.csv".

Pro měření výkonu stačí použít spustitelný soubor "perf_hist_estimate" se stejnými parametry. Naměřené výkonnostní hodnoty (v ms) jsou pak k dispozici v souboru "<název_obrázku>.accur+perf.csv". Pro vizualizaci vybraných pixelů (např. pro zkoumání vhodnosti parametrů) je třeba použít spustitelný soubor "show_hist_estimate". V tomto případě neprobíhá při nezvolení konkrétní metody celý cyklus, aby se zamezilo vytvoření velkého množství oken.
