# ZPO projekt

### Zadanie: Odhad histogramu obrazu
Autori: 

* Tomáš Hajdík
* David Novák

### Popis implementácie
*  hist_estimate.cpp - hlavný súbor implementácie
*  methods.cpp, methods.h - implementované metódy odhadu histogramu
*  settings.h - konfigurácia východiskových hodnôt parametrov
*  helper_functions.cpp - pomocné funkcie pre: vyprázdnanie cache, výpočet presnosti, vykreslenie histogramu, formátovanie csv
*  Makefile - preklad aplikácie

### Preklad aplikáce

Preklad aplikácie sa spustí pomocou príkazu *make* v príkazovom riadku.
Po preložení sú vygenerované tri varianty binárneho súboru `hist_estimate`, `show_hist_estimate` a `perf_hist_estimate`.

### Meranie presnosti odhadu histogramu

Odkaz na výsledkové grafy porovnávajúce presnosť jednotlivých metód:

[https://docs.google.com/spreadsheets/d/1KmFGDkt5QUsXWDJ41iuw7rQ9eTYtc0TLsmeG6keupTo/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1KmFGDkt5QUsXWDJ41iuw7rQ9eTYtc0TLsmeG6keupTo/edit?usp=sharing)
