/*
 * ZPO projekt
 * Odhad histogramu obrazu
 * Autoři: David Novák, xnovak1m; Tomáš Hajdík, xhajdi01
 *
 */

#include "main.h"
#include "settings.h"
#include "helper_functions.h"
#include "methods.h"

using namespace std;
using namespace std::chrono;
using namespace cv;

struct est_method {
	int id;
	string name;
};

double MAXrefHist = 0; // 3988

map<string, pair<int, calc_duration>> perf;
map<string, struct est_method>methods;

// funkce pro zaznamenávání výsledků měření
void record_perf(string key, calc_duration dur)
{
	pair<int, calc_duration> old;

	try
	{
		old = perf.at(key);  // zkontrolovat, jestli už tento prvek v Map je

		old.second += dur;
		old.first++;
		perf.erase(key);  // smazat starou hodnotu
		perf.insert(pair<string, pair<int, calc_duration>>(key, old));
	}
	catch (...)
	{
		perf.insert(pair<string, pair<int, calc_duration>>(key, pair<int, calc_duration>(1,dur)));
	}

}

// zaznamenání minimálního výsledku (vhodné pro metody bez náhodnosti - odstíní zpomalení vlivem aktivity OS)
void record_perf_min(string key, calc_duration dur)
{
	pair<int, calc_duration> old;

	try
	{
		old = perf.at(key);  // zkontrolovat, jestli už tento prvek v Map je

		if (dur > old.second)
			dur = old.second;

		perf.erase(key);  // smazat starou hodnotu
	}
	catch (...) {}

	perf.insert(pair<string, pair<int, calc_duration>>(key, pair<int, calc_duration>(1,dur)));
}

void print_methods()
{
	for (auto i = methods.begin(); i != methods.end(); i++)
		cout << i->first << ": " << i->second.name << endl;
}

void print_help()
{
	cout << "Histogram estimation methods - help" << endl;
	cout << "###################################" << endl;
	#if perfMeasure
		cout << "This is binary version for performance measurement. Count of pixels used for estimate is not available." << endl;
	#endif
	#if showComputedPixImage
		cout << "This binary version shows visualization of pixels chosen for histogram estimation (white on black background)." << endl;
	#endif
	cout << endl << "Available options:" << endl;
	cout << "--file (-f) <filepath> (REQUIRED)" << endl;
	cout << "--method (-m) <name> (one of: M1-M8, P1-P4, P1V, P3V)" << endl;
	cout << "--iterations <integer> (changes the number of iterations for measuring)" << endl;
	cout << "--visual (-v) (displays visual reprezentation of histogram and its estimation)" << endl;
	cout << "--print (prints normalized values of histogram and its estimation to stdout)" << endl;
	cout << "--showPicture (shows input picture)" << endl;
	cout << "--help (-h) (displays this text)" << endl;
	cout << "--nth (-n) <integer> (parameter for methods M1-M3)" << endl;
	cout << "--rand (-a) <double> (parameter for methods M4-M7)" << endl;
	cout << "--sigma (-s) <double> (parameter for method M7)" << endl;
	cout << "--clusters (-c) <integer> (parameter for method M7)" << endl;
	cout << "--cache (-i) <integer> (parameter for methods P1 and P1V - size of cache-line step)" << endl;
	cout << "--cacheMin (-j) <integer> (parameter for methods P2-P4 and P3V - min cache-line step)" << endl;
	cout << "--cacheMax (-k) <integer> (parameter for methods P2-P4 and P3V - max cache-line step)" << endl;
	cout << "--pixel (-p) <integer> (parameter for methods P1-P3 - size of pixel step -> every pth pixel in cache-line is used)" << endl;
	cout << "--pixelMin (-q) <integer> (parameter for methods P1V and P3V - min pixel step)" << endl;
	cout << "--pixelMax (-r) <integer> (parameter for methods P1V and P3V - max pixel step)" << endl;
	cout << "--thresholdPixel (-t) <integer> (parameter for method P4 - threshold for pixel similarity)" << endl;
	cout << "--thresholdCache (-u) <integer> (parameter for method P4 - threshold for change of cache-line step)" << endl;

	cout << endl << "Available methods:" << endl;
	print_methods();
}

int main(int argc, char *argv[])
{
	string imagePath;
	string method = "";

	int iterations = ITERATIONS;
	bool visual = false;
	bool print = false;
	bool showPicture = false;

	int Nth_arg = ARG_NTH;
	double Rnd_arg = ARG_RAND;
	int cntOfClusters = ARG_CLUSTERS;
	double sigma = ARG_SIGMA;

	int cacheStepSize = ARG_CACHE;
	int pixelStepSize = ARG_PIXEL;

	int cacheStepSizeMin = ARG_CACHE_MIN;
	int cacheStepSizeMax = ARG_CACHE_MAX;
	int pixelStepSizeMin = ARG_PIXEL_MIN;
	int pixelStepSizeMax = ARG_PIXEL_MAX;

	int heur_pixel_threshold = ARG_THR_PIXEL;
	int heur_step_threshold = ARG_THR_CACHE;

	methods.insert(pair<string, struct est_method>("M1", {1, "Every Nth pixel"}));
	methods.insert(pair<string, struct est_method>("M2", {2, "Every Nth row"}));
	methods.insert(pair<string, struct est_method>("M3", {3, "Every Nth column"}));
	methods.insert(pair<string, struct est_method>("M4", {4, "Random pixels (uniform)"}));
	methods.insert(pair<string, struct est_method>("M5", {5, "Random pixels (uniform) without duplicities"}));
	methods.insert(pair<string, struct est_method>("M6", {6, "Random pixels (gauss)"}));
	methods.insert(pair<string, struct est_method>("M7", {7, "Random pixels (cluster)"}));
	methods.insert(pair<string, struct est_method>("M8", {8, "Nth pixel with random step"}));
	methods.insert(pair<string, struct est_method>("P1",  {9, "Every Nth cache-line"}));
	methods.insert(pair<string, struct est_method>("P1V", {10, "Every Nth cache-line with variable pixel step"}));
	methods.insert(pair<string, struct est_method>("P2",  {11, "Nth cache-line with variable step"}));
	methods.insert(pair<string, struct est_method>("P3",  {12, "Nth cache-line with random step"}));
	methods.insert(pair<string, struct est_method>("P3V", {13, "Nth cache-line with random step and random pixel step"}));
	methods.insert(pair<string, struct est_method>("P4",  {14, "Heuristic cache-line and pixel step selection"}));

	bool ready = false;
	const char* const short_opts = "f:m:n:s:c:a:i:j:k:p:q:r:t:u:vh";
	const option long_opts[] = {
		{"file", required_argument, nullptr, 'f'},
		{"method", required_argument, nullptr, 'm'},
		{"nth", required_argument, nullptr, 'n'},
		{"sigma", required_argument, nullptr, 's'},
		{"clusters", required_argument, nullptr, 'c'},
		{"rand", required_argument, nullptr, 'a'},
		{"cache", required_argument, nullptr, 'i'},
		{"cacheMin", required_argument, nullptr, 'j'},
		{"cacheMax", required_argument, nullptr, 'k'},
		{"pixel", required_argument, nullptr, 'p'},
		{"pixelMin", required_argument, nullptr, 'q'},
		{"pixelMax", required_argument, nullptr, 'r'},
		{"thresholdPixel", required_argument, nullptr, 't'},
		{"thresholdCache", required_argument, nullptr, 'u'},
		{"iterations", required_argument, nullptr, 'x'},
		{"visual", no_argument, nullptr, 'v'},
		{"print", no_argument, nullptr, 1},
		{"showPicture", no_argument, nullptr, 2},
		{"help", no_argument, nullptr, 'h'},
		{nullptr, no_argument, nullptr, 0}
	};

	while (true)
	{
		const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);

		if (opt == -1)
			break;

		switch (opt)
		{
			case 'f':
				imagePath = optarg;
				ready = true;
				break;

			case 'm':
				method = optarg;
				break;

			case 's':
				sigma = stod(optarg);
				break;

			case 'n':
				Nth_arg = stoi(optarg);
				break;

			case 'c':
				cntOfClusters = stoi(optarg);
				break;

			case 'a':
				Rnd_arg = stod(optarg);
				break;

			case 'i':
				cacheStepSize = stoi(optarg);
				break;

			case 'j':
				cacheStepSizeMin = stoi(optarg);
				break;

			case 'k':
				cacheStepSizeMax = stoi(optarg);
				break;

			case 'p':
				pixelStepSize = stoi(optarg);
				break;

			case 'q':
				pixelStepSizeMin = stoi(optarg);
				break;

			case 'r':
				pixelStepSizeMax = stoi(optarg);
				break;

			case 't':
				heur_pixel_threshold = stoi(optarg);
				break;

			case 'u':
				heur_step_threshold = stoi(optarg);
				break;

			case 'x':
				iterations = stoi(optarg);
				break;

			case 'v':
				visual = true;
				break;

			case 1:
				print = true;
				break;

			case 2:
				showPicture = true;
				break;

			case 'h':
			case '?':
				print_help();
				return 0;
				break;
		}
	}

	if (!ready)
	{
		cout << "ERROR: Missing file argument." << endl;
		print_help();
		return -1;
	}

	// načíta obrázok zadaný prvým argumentom v grayscale
	Mat imageGrayScale = cv::imread(imagePath, CV_LOAD_IMAGE_GRAYSCALE);

	// kontrola načítania obrázka
	if( imageGrayScale.empty() ) {
		std::cout << "ERROR: Cannot load input image:" << imagePath << std::endl;
		return -1;
	}
	
	if (showPicture)
	{
		namedWindow( "Histogram estimation methods", WINDOW_AUTOSIZE );
		imshow( "Histogram estimation methods", imageGrayScale );
	}

	// deklarácia referenčného histogramu
	vector<double> histogram(CNT_OF_HIST_BINS, 0);
	vector<double> hist_perf(CNT_OF_HIST_BINS, 0);
	// deklarácia histogramov pre jednotlivé metódy
	vector<double> histM1(CNT_OF_HIST_BINS, 0);
	vector<double> histM2(CNT_OF_HIST_BINS, 0);
	vector<double> histM3(CNT_OF_HIST_BINS, 0);
	vector<double> histM4(CNT_OF_HIST_BINS, 0);
	vector<double> histM5(CNT_OF_HIST_BINS, 0);
	vector<double> histM6(CNT_OF_HIST_BINS, 0);
	vector<double> histM7(CNT_OF_HIST_BINS, 0);
	vector<double> histM8(CNT_OF_HIST_BINS, 0);
	vector<double> histP1(CNT_OF_HIST_BINS, 0);
	vector<double> histP1V(CNT_OF_HIST_BINS, 0);
	vector<double> histP2(CNT_OF_HIST_BINS, 0);
	vector<double> histP3(CNT_OF_HIST_BINS, 0);
	vector<double> histP3V(CNT_OF_HIST_BINS, 0);
	vector<double> histP4(CNT_OF_HIST_BINS, 0);

	// výpočet referenčného histogramu
	computeHistogram(imageGrayScale, histogram);
	MAXrefHist = findMaxInHistogram(histogram);

	// normalizácia referenčného histogramu
	normalize(histogram, histogram, 1.0, 0.0, NORM_L1);

	if (method != "")
	{
		vector<double> estimate(CNT_OF_HIST_BINS, 0);
		int pixel_count = 0;

		try {
			struct est_method chosen = methods.at(method);
			cout << method << ": " << chosen.name << endl;


			switch (chosen.id)
			{
				case 1:
					cout << "nth: " << Nth_arg << endl;
					pixel_count = M1_NthPixelHistogram(imageGrayScale, estimate, Nth_arg);
					break;

				case 2:
					cout << "nth: " << Nth_arg << endl;
					pixel_count = M2_NthRowHistogram(imageGrayScale, estimate, Nth_arg);
					break;

				case 3:
					cout << "nth: " << Nth_arg << endl;
					pixel_count = M3_NthColHistogram(imageGrayScale, estimate, Nth_arg);
					break;

				case 4:
					cout << "rnd: " << Rnd_arg << endl;
					pixel_count = M4_RndUnifHistogram(imageGrayScale, estimate, Rnd_arg);
					break;

				case 5:
					cout << "rnd: " << Rnd_arg << endl;
					pixel_count = M5_RndUnifNoDuplicHistogram(imageGrayScale, estimate, Rnd_arg);
					break;

				case 6:
					cout << "rnd: " << Rnd_arg << endl;
					pixel_count = M6_RndGausHistogram(imageGrayScale, estimate, Rnd_arg);
					break;

				case 7:
					cout << "rnd: " << Rnd_arg << endl;
					cout << "Number of clusters: " << cntOfClusters << endl;
					cout << "Sigma: " << sigma << endl;
					pixel_count = M7_RndUnifClustHistogram(imageGrayScale, estimate, Rnd_arg, cntOfClusters, sigma);
					break;

				case 8:
					cout << "Min pixel step: " << pixelStepSizeMin << endl;
					cout << "Max pixel step: " << pixelStepSizeMax << endl;
					pixel_count = M8_RandomNthHistogram(imageGrayScale, estimate, pixelStepSizeMin, pixelStepSizeMax);
					break;

				case 9:
					cout << "Cache-line step: " << cacheStepSize << endl;
					cout << "Pixel step: " << pixelStepSize << endl;
					pixel_count = P1_CacheLinesHistogram(imageGrayScale, estimate, cacheStepSize, pixelStepSize);
					break;

				case 10:
					cout << "Cache-line step: " << cacheStepSize << endl;
					cout << "Min pixel step: " << pixelStepSizeMin << endl;
					cout << "Max pixel step: " << pixelStepSizeMax << endl;
					pixel_count = P1_CacheLinesHistogram(imageGrayScale, estimate, cacheStepSize, pixelStepSizeMin, pixelStepSizeMax);
					break;

				case 11:
					cout << "Cache-line min step: " << cacheStepSizeMin << endl;
					cout << "Cache-line max step: " << cacheStepSizeMax << endl;
					cout << "Pixel step: " << pixelStepSize << endl;
					pixel_count = P2_VariableCacheLinesHistogram(imageGrayScale, estimate, cacheStepSizeMin, cacheStepSizeMax, pixelStepSize);
					break;

				case 12:
					cout << "Cache-line min step: " << cacheStepSizeMin << endl;
					cout << "Cache-line max step: " << cacheStepSizeMax << endl;
					cout << "Pixel step: " << pixelStepSize << endl;
					pixel_count = P3_RandomCacheLinesHistogram(imageGrayScale, estimate, cacheStepSizeMin, cacheStepSizeMax, pixelStepSize);
					break;

				case 13:
					cout << "Cache-line min step: " << cacheStepSizeMin << endl;
					cout << "Cache-line max step: " << cacheStepSizeMax << endl;
					cout << "Min pixel step: " << pixelStepSizeMin << endl;
					cout << "Max pixel step: " << pixelStepSizeMax << endl;
					pixel_count = P3_RandomCacheLinesHistogram(imageGrayScale, estimate, cacheStepSizeMin, cacheStepSizeMax, pixelStepSizeMin, pixelStepSizeMax);
					break;

				case 14:
					cout << "Cache-line min step: " << cacheStepSizeMin << endl;
					cout << "Cache-line max step: " << cacheStepSizeMax << endl;
					cout << "Pixel step: " << pixelStepSize << endl;
					cout << "Pixel step change threshold: " << heur_pixel_threshold << endl;
					cout << "Cache-line step change threshold: " << heur_step_threshold << endl;
					pixel_count = P4_HeuristicCacheLinesHistogram(imageGrayScale, estimate, cacheStepSizeMin, cacheStepSizeMax, pixelStepSize, heur_pixel_threshold, heur_step_threshold);
					break;
			}

		}
		catch (...) {
			cout << "ERROR: Chosen method doesn't exist. Choose from following:" << endl;
			print_methods();
			return -1;
		}


		double accur = compute_accur(histogram, estimate);
		printf("Accuracy: %0.12f\n", accur);

		#if perfMeasure
			for (int i = 0; i < iterations-1; i++)
				M1_NthPixelHistogram(imageGrayScale, histM1, Nth_arg);

			calc_duration est_perf = perf.begin()->second.second / perf.begin()->second.first;
			printf("Time: %0.12f ms\n", est_perf.count());
		#else
			printf("Number of pixels used in calculation: %d\n", pixel_count);
		#endif

		if (print)
		{
			cout << endl << "Histogram (normalized): " << endl;
			printHistogram(histogram);
			cout << endl << "Estimate (normalized): " << endl;
			printHistogram(estimate);
		}

		if (visual)
		{
			showHistogram(histogram, "Histogram");
			showHistogram(estimate, "Estimate");
			cout << endl << "Press any key to end." << endl;
			waitKey(0);
		}
		else if (showPicture)
			waitKey(0);

		#if showComputedPixImage
			if (!visual && !showPicture)
				waitKey(0);
		#endif

		return 0; // end program here
	}

#if accurMeasure
	FILE * fp;

	// názov výstupného súboru
	string filePath = imagePath.substr(imagePath.find_last_of("/\\")+1);
	filePath = filePath.substr(0, filePath.find_last_of("."));
	filePath = filePath.append(".accur.csv");
	filePath = filePath.insert(0, "csv/");

 	fp = fopen (filePath.c_str(),"w+");
 	if (!fp)
 	{
 		cout << "File: " << filePath.c_str() << " cannot be opened." << endl;
 		exit(1);
 	}

	FILE *f_perf;

	string filePath_perf = imagePath.substr(imagePath.find_last_of("/\\")+1);
	filePath_perf = filePath_perf.substr(0, filePath_perf.find_last_of("."));
	filePath_perf = filePath_perf.append(".accur+perf.csv");
	filePath_perf = filePath_perf.insert(0, "csv/");

	f_perf = fopen(filePath_perf.c_str(),"w");
 	if (!f_perf)
 	{
 		cout << "File: " << filePath_perf.c_str() << " cannot be opened." << endl;
 		exit(1);
 	}




	fprintf(fp, "Elements, M1_NthPixelHistogram, M2_NthRowHistogram, M3_NthColHistogram, M4_RndUnifHistogram, M5_RndUnifNoDuplicHistogram, M6_RndGausHistogram, M7_RndUnifClustHistogram, M8_RandomNthHistogram, P1_CacheLinesHistogram, P1_CacheLinesHistogramVariablePixelStep, P2_VariableCacheLinesHistogram, P3_RandomCacheLinesHistogram, P3_RandomCacheLinesRandomPixelStepHistogram, P4_HeuristicCacheLinesHistogram\n");

	fprintf(f_perf, "Nth_arg, M1_NthPixelHistogram, perf, M2_NthRowHistogram, perf, M3_NthColHistogram, perf, Rnd_arg, M4_RndUnifHistogram, perf, M5_RndUnifNoDuplicHistogram, perf, M6_RndGausHistogram, perf, M7_RndUnifClustHistogram, perf, M8_RandomNthHistogram, perf, cacheStepSize, pixelStepSize, P1_CacheLinesHistogram, perf, pixelStepSizeMin, pixelStepSizeMax, P1_CacheLinesHistogramVariablePixelStep, perf, cacheStepSizeMin, cacheStepSizeMax, P2_VariableCacheLinesHistogram, perf, P3_RandomCacheLinesHistogram, perf, P3_RandomCacheLinesRandomPixelStepHistogram, perf, heur_pixel_threshold, heur_step_threshold, P4_HeuristicCacheLinesHistogram, perf, reference performance\n");

	cacheStepSize = 1;
	cacheStepSizeMin = 1;
	cacheStepSizeMax = 5;

	pixelStepSize = 4;
	pixelStepSizeMin = 2;
	pixelStepSizeMax = 8;

	cntOfClusters = 1000;
	sigma = 50.0;

	heur_pixel_threshold = 5;
	heur_step_threshold = 20;

	perf.clear();

	// změřit referenční implemementaci histogramu
	#if perfMeasure
		for (int i = 0; i < 10*iterations; i++)
			computeHistogram(imageGrayScale, hist_perf);

		calc_duration reference_perf = perf.begin()->second.second / perf.begin()->second.first;
		printf("Reference histogram time: %0.12f ms\n", reference_perf.count());
	#endif

	int iteration = 0;

	for (int i=1; i <= 100; i < 10 ? i++ : i+=10)
	{
		iteration++;
		double accur = 0.0;
		perf.clear();   // odstranění předchozích naměřených hodnot

		// prepočet pre jednotlivé metódy aby poč. pix klesal rovnako pre každú
		Nth_arg = i*10;
		Rnd_arg = 1.0/((double)i*10.0);

		fprintf(fp,"%d,", Nth_arg);
		fprintf(f_perf,"%d,", Nth_arg);

		#if perfMeasure
		cout << "Iteration " << iteration << " out of 19..." << endl;
		for (int i = 0; i < iterations; i++)
		{
			M1_NthPixelHistogram(imageGrayScale, histM1, Nth_arg);
			M2_NthRowHistogram(imageGrayScale, histM2, Nth_arg);
			M3_NthColHistogram(imageGrayScale, histM3, Nth_arg);
			M4_RndUnifHistogram(imageGrayScale, histM4, Rnd_arg);
			M5_RndUnifNoDuplicHistogram(imageGrayScale, histM5, Rnd_arg);
			M6_RndGausHistogram(imageGrayScale, histM6, Rnd_arg);
			M7_RndUnifClustHistogram(imageGrayScale, histM7, Rnd_arg, cntOfClusters, sigma);
			M8_RandomNthHistogram(imageGrayScale, histM8, Nth_arg/2, Nth_arg*2);
			P1_CacheLinesHistogram(imageGrayScale, histP1, cacheStepSize, pixelStepSize);
			P1_CacheLinesHistogram(imageGrayScale, histP1V, cacheStepSize, pixelStepSizeMin, pixelStepSizeMax);
			P2_VariableCacheLinesHistogram(imageGrayScale, histP2, cacheStepSizeMin, cacheStepSizeMax, pixelStepSize);
			P3_RandomCacheLinesHistogram(imageGrayScale, histP3, cacheStepSizeMin, cacheStepSizeMax, pixelStepSize);
			P3_RandomCacheLinesHistogram(imageGrayScale, histP3V, cacheStepSizeMin, cacheStepSizeMax, pixelStepSizeMin, pixelStepSizeMax);
			P4_HeuristicCacheLinesHistogram(imageGrayScale, histP4, cacheStepSizeMin, cacheStepSizeMax, pixelStepSize, heur_pixel_threshold, heur_step_threshold);
		}
		auto perf_iter = perf.begin();
		calc_duration perf_time;
		#endif

		#ifndef perfMeasure
			// výpočet histogramu pomocou implementovaných metód
			// a výpis počtu pixelov využitých k výpočtu
			cout << "ITERATION " << iteration << endl << "***************************" << endl;
			cout << "Number of pixels used in calculation - computeHistogram: " << computeHistogram(imageGrayScale, hist_perf) << endl;
			cout << "Nth_arg: " << Nth_arg << ", Rnd_arg: " << Rnd_arg << endl;
			cout << "Number of pixels used in calculation - M1_NthPixelHistogram: " << M1_NthPixelHistogram(imageGrayScale, histM1, Nth_arg) << endl;
			cout << "Number of pixels used in calculation - M2_NthRowHistogram: " << M2_NthRowHistogram(imageGrayScale, histM2, Nth_arg) << endl;
			cout << "Number of pixels used in calculation - M3_NthColHistogram: " << M3_NthColHistogram(imageGrayScale, histM3, Nth_arg) << endl;
			cout << "Number of pixels used in calculation - M4_RndUnifHistogram: " << M4_RndUnifHistogram(imageGrayScale, histM4, Rnd_arg) << endl;
			cout << "Number of pixels used in calculation - M5_RndUnifNoDuplicHistogram: " << M5_RndUnifNoDuplicHistogram(imageGrayScale, histM5, Rnd_arg) << endl;
			cout << "Number of pixels used in calculation - M6_RndGausHistogram: " << M6_RndGausHistogram(imageGrayScale, histM6, Rnd_arg) << endl;
			cout << "Number of pixels used in calculation - M7_RndUnifClustHistogram: " << M7_RndUnifClustHistogram(imageGrayScale, histM7, Rnd_arg, cntOfClusters, sigma) << endl;
			cout << "Number of pixels used in calculation - M8_RandomNthHistogram: " << M8_RandomNthHistogram(imageGrayScale, histM8, Nth_arg/2, Nth_arg*2) << endl;
			cout << "cacheStepSize: " << cacheStepSize << ", cacheStepSizeMin: " << cacheStepSizeMin << ", cacheStepSizeMax: " << cacheStepSizeMax << endl;
			cout << "pixelStepSize: " << pixelStepSize << ", pixelStepSizeMin: " << pixelStepSizeMin << ", pixelStepSizeMax: " << pixelStepSizeMax << endl;
			cout << "Number of pixels used in calculation - P1_CacheLinesHistogram: " << P1_CacheLinesHistogram(imageGrayScale, histP1, cacheStepSize, pixelStepSize) << endl;
			cout << "Number of pixels used in calculation - P1_CacheLinesHistogramVariablePixelStep: " << P1_CacheLinesHistogram(imageGrayScale, histP1V, cacheStepSize, pixelStepSizeMin, pixelStepSizeMax) << endl;
			cout << "Number of pixels used in calculation - P2_VariableCacheLinesHistogram: " << P2_VariableCacheLinesHistogram(imageGrayScale, histP2, cacheStepSizeMin, cacheStepSizeMax, pixelStepSize) << endl;
			cout << "Number of pixels used in calculation - P3_RandomCacheLinesHistogram: " << P3_RandomCacheLinesHistogram(imageGrayScale, histP3, cacheStepSizeMin, cacheStepSizeMax, pixelStepSize) << endl;
			cout << "Number of pixels used in calculation - P3_RandomCacheLinesRandomPixelStepHistogram: " << P3_RandomCacheLinesHistogram(imageGrayScale, histP3V, cacheStepSizeMin, cacheStepSizeMax, pixelStepSizeMin, pixelStepSizeMax) << endl;
			cout << "heur_pixel_threshold: " << heur_pixel_threshold << ", heur_step_threshold: " << heur_step_threshold << endl;
			cout << "Number of pixels used in calculation - P4_HeuristicCacheLinesHistogram: " << P4_HeuristicCacheLinesHistogram(imageGrayScale, histP4, cacheStepSizeMin, cacheStepSizeMax, pixelStepSize, heur_pixel_threshold, heur_step_threshold) << endl;
			cout << endl;

		#endif

		// 1 metóda - experimentácia s argumentami
		// výpočet histogramu pomocou implementovanej metódy
		M1_NthPixelHistogram(imageGrayScale, histM1, Nth_arg);
		accur = compute_accur(histogram, histM1);
		fprintf(fp,"%0.12f,", accur);
		fprintf(f_perf,"%0.12f,", accur);

		#if perfMeasure
			perf_time = perf_iter->second.second / perf_iter->second.first;
			fprintf(f_perf,"%0.12f,", perf_time.count());
			perf_iter++;
		#else
			fprintf(f_perf,",");
		#endif

		// 2 metóda - experimentácia s argumentami
		// výpočet histogramu pomocou implementovanej metódy
		M2_NthRowHistogram(imageGrayScale, histM2, Nth_arg);
		accur = compute_accur(histogram, histM2);
		fprintf(fp,"%0.12f,", accur);
		fprintf(f_perf,"%0.12f,", accur);

		#if perfMeasure
			perf_time = perf_iter->second.second / perf_iter->second.first;
			fprintf(f_perf,"%0.12f,", perf_time.count());
			perf_iter++;
		#else
			fprintf(f_perf,",");
		#endif

		// 3 metóda - experimentácia s argumentami
		// výpočet histogramu pomocou implementovanej metódy
		M3_NthColHistogram(imageGrayScale, histM3, Nth_arg);
		accur = compute_accur(histogram, histM3);
		fprintf(fp,"%0.12f,", accur);
		fprintf(f_perf,"%0.12f,", accur);

		#if perfMeasure
			perf_time = perf_iter->second.second / perf_iter->second.first;
			fprintf(f_perf,"%0.12f,", perf_time.count());
			perf_iter++;
		#else
			fprintf(f_perf,",");
		#endif

		fprintf(f_perf,"%0.3f,", Rnd_arg);
		// 4 metóda - experimentácia s argumentami
		// výpočet histogramu pomocou implementovanej metódy
		accur = 0;
		for (int i = 0; i < iterations; i++)
		{
			M4_RndUnifHistogram(imageGrayScale, histM4, Rnd_arg);
			accur += compute_accur(histogram, histM4);
		}
		accur /= iterations;
		fprintf(fp,"%0.12f,", accur);
		fprintf(f_perf,"%0.12f,", accur);

		#if perfMeasure
			perf_time = perf_iter->second.second / perf_iter->second.first;
			fprintf(f_perf,"%0.12f,", perf_time.count());
			perf_iter++;
		#else
			fprintf(f_perf,",");
		#endif

		// 5 metóda - experimentácia s argumentami
		// výpočet histogramu pomocou implementovanej metódy
		accur = 0;
		for (int i = 0; i < iterations; i++)
		{
			M5_RndUnifNoDuplicHistogram(imageGrayScale, histM5, Rnd_arg);
			accur += compute_accur(histogram, histM5);
		}
		accur /= iterations;
		fprintf(fp,"%0.12f,", accur);
		fprintf(f_perf,"%0.12f,", accur);

		#if perfMeasure
			perf_time = perf_iter->second.second / perf_iter->second.first;
			fprintf(f_perf,"%0.12f,", perf_time.count());
			perf_iter++;
		#else
			fprintf(f_perf,",");
		#endif

		// 6 metóda - experimentácia s argumentami
		// výpočet histogramu pomocou implementovanej metódy
		accur = 0;
		for (int i = 0; i < iterations; i++)
		{
			M6_RndGausHistogram(imageGrayScale, histM6, Rnd_arg);
			accur += compute_accur(histogram, histM6);
		}
		accur /= iterations;
		fprintf(fp,"%0.12f,", accur);
		fprintf(f_perf,"%0.12f,", accur);

		#if perfMeasure
			perf_time = perf_iter->second.second / perf_iter->second.first;
			fprintf(f_perf,"%0.12f,", perf_time.count());
			perf_iter++;
		#else
			fprintf(f_perf,",");
		#endif

		cntOfClusters = 1000;
		sigma = 50.0;
		// 7 metóda - experimentácia s argumentami
		// výpočet histogramu pomocou implementovanej metódy
		accur = 0;
		for (int i = 0; i < iterations; i++)
		{
			M7_RndUnifClustHistogram(imageGrayScale, histM7, Rnd_arg, cntOfClusters, sigma);
			accur += compute_accur(histogram, histM7);
		}
		accur /= iterations;
		fprintf(fp,"%0.12f,", accur);
		fprintf(f_perf,"%0.12f,", accur);

		#if perfMeasure
			perf_time = perf_iter->second.second / perf_iter->second.first;
			fprintf(f_perf,"%0.12f,", perf_time.count());
			perf_iter++;
		#else
			fprintf(f_perf,",");
		#endif

		accur = 0;
		for (int i = 0; i < iterations; i++)
		{
			M8_RandomNthHistogram(imageGrayScale, histM8, Nth_arg/2, Nth_arg*2);
			accur += compute_accur(histogram, histM8);
		}
		accur /= iterations;
		fprintf(fp,"%0.12f,", accur);
		fprintf(f_perf,"%0.12f,", accur);

		#if perfMeasure
			perf_time = perf_iter->second.second / perf_iter->second.first;
			fprintf(f_perf,"%0.12f,", perf_time.count());
			perf_iter++;
		#else
			fprintf(f_perf,",");
		#endif

		fprintf(f_perf,"%d, %d,", cacheStepSize, pixelStepSize);

		P1_CacheLinesHistogram(imageGrayScale, histP1, cacheStepSize, pixelStepSize);
		accur = compute_accur(histogram, histP1);
		fprintf(fp,"%0.12f,", accur);
		fprintf(f_perf,"%0.12f,", accur);

		#if perfMeasure
			perf_time = perf_iter->second.second / perf_iter->second.first;
			fprintf(f_perf,"%0.12f,", perf_time.count());
			perf_iter++;
		#else
			fprintf(f_perf,",");
		#endif

		fprintf(f_perf,"%d, %d,", pixelStepSizeMin, pixelStepSizeMax);

		P1_CacheLinesHistogram(imageGrayScale, histP1V, cacheStepSize, pixelStepSizeMin, pixelStepSizeMax);
		accur = compute_accur(histogram, histP1V);
		fprintf(fp,"%0.12f,", accur);
		fprintf(f_perf,"%0.12f,", accur);

		#if perfMeasure
			perf_time = perf_iter->second.second / perf_iter->second.first;
			fprintf(f_perf,"%0.12f,", perf_time.count());
			perf_iter++;
		#else
			fprintf(f_perf,",");
		#endif


		fprintf(f_perf,"%d, %d,", cacheStepSizeMin, cacheStepSizeMax);


		P2_VariableCacheLinesHistogram(imageGrayScale, histP2, cacheStepSizeMin, cacheStepSizeMax, pixelStepSize);
		accur = compute_accur(histogram, histP2);
		fprintf(fp,"%0.12f,", accur);
		fprintf(f_perf,"%0.12f,", accur);

		#if perfMeasure
			perf_time = perf_iter->second.second / perf_iter->second.first;
			fprintf(f_perf,"%0.12f,", perf_time.count());
			perf_iter++;
		#else
			fprintf(f_perf,",");
		#endif

		accur = 0;
		for (int i = 0; i < iterations; i++)
		{
			P3_RandomCacheLinesHistogram(imageGrayScale, histP3, cacheStepSizeMin, cacheStepSizeMax, pixelStepSize);
			accur += compute_accur(histogram, histP3);
		}
		accur /= iterations;
		fprintf(fp,"%0.12f,", accur);
		fprintf(f_perf,"%0.12f,", accur);

		#if perfMeasure
			perf_time = perf_iter->second.second / perf_iter->second.first;
			fprintf(f_perf,"%0.12f,", perf_time.count());
			perf_iter++;
		#else
			fprintf(f_perf,",");
		#endif

		//P3_RandomCacheLinesRandomPixelStepHistogram
		accur = 0;
		for (int i = 0; i < iterations; i++)
		{
			P3_RandomCacheLinesHistogram(imageGrayScale, histP3V, cacheStepSizeMin, cacheStepSizeMax, pixelStepSizeMin, pixelStepSizeMax);
			accur += compute_accur(histogram, histP3V);
		}
		accur /= iterations;
		fprintf(fp,"%0.12f,", accur);
		fprintf(f_perf,"%0.12f,", accur);

		#if perfMeasure
			perf_time = perf_iter->second.second / perf_iter->second.first;
			fprintf(f_perf,"%0.12f,", perf_time.count());
			perf_iter++;
		#else
			fprintf(f_perf,",");
		#endif

		fprintf(f_perf,"%d, %d,", heur_pixel_threshold, heur_step_threshold);

		P4_HeuristicCacheLinesHistogram(imageGrayScale, histP4, cacheStepSizeMin, cacheStepSizeMax, pixelStepSize, heur_pixel_threshold, heur_step_threshold);
		accur = compute_accur(histogram, histP4);
		fprintf(fp,"%0.12f", accur);
		fprintf(f_perf,"%0.12f", accur);

		#if perfMeasure
			perf_time = perf_iter->second.second / perf_iter->second.first;
			fprintf(f_perf,",%0.12f", perf_time.count());
			perf_iter++;
		#else
			fprintf(f_perf,",");
		#endif

		// refererenční výkon
		#if perfMeasure
			fprintf(f_perf,",%0.12f", reference_perf.count());
		#endif

		fprintf(fp,"\n");
		fprintf(f_perf,"\n");

		cacheStepSize += 1;
		heur_pixel_threshold++;

		if (i < 10)
		{
			heur_step_threshold--;
			cacheStepSizeMax += 5;
		}
		else
		{
			pixelStepSize += 1;
			heur_step_threshold -= 2;
			if (i/10 % 2)
			{
				cacheStepSizeMax += 15;
				cacheStepSizeMin += 2;
				pixelStepSizeMin += 1;
				pixelStepSizeMax += 1;
			}
			else
			{
				cacheStepSizeMax += 10;
				cacheStepSizeMin += 1;
				pixelStepSizeMax += 2;
			}
		}

		if (i == 10)
		{
			heur_step_threshold = 23;
		}

		#if showComputedPixImage
			cout << "Running show_hist_estimate version - ending after first iteration to prevent window spam." << endl;
			break; // prevent TON of windows
		#endif

	}

	// výpis histogramu na stdout
	if (print)
	{
		cout << endl << "Histogram (normalized): " << endl;
		printHistogram(histogram);
		cout << endl << "M1 (normalized): " << endl;
		printHistogram(histM1);
		cout << endl << "M2 (normalized): " << endl;
		printHistogram(histM2);
		cout << endl << "M3 (normalized): " << endl;
		printHistogram(histM3);
		cout << endl << "M4 (normalized): " << endl;
		printHistogram(histM4);
		cout << endl << "M5 (normalized): " << endl;
		printHistogram(histM5);
		cout << endl << "M6 (normalized): " << endl;
		printHistogram(histM6);
		cout << endl << "M7 (normalized): " << endl;
		printHistogram(histM7);
		cout << endl << "M8 (normalized): " << endl;
		printHistogram(histM8);
		cout << endl << "P1 (normalized): " << endl;
		printHistogram(histP1);
		cout << endl << "P1V (normalized): " << endl;
		printHistogram(histP1V);
		cout << endl << "P2 (normalized): " << endl;
		printHistogram(histP2);
		cout << endl << "P3 (normalized): " << endl;
		printHistogram(histP3);
		cout << endl << "P3V (normalized): " << endl;
		printHistogram(histP3V);
		cout << endl << "P4 (normalized): " << endl;
		printHistogram(histP4);
	}

	// nahradí čiarky za medzery a bodky za čiarky pre jednoduchsi import
	fclose (fp);
	ifstream t(filePath.c_str());
	string str((istreambuf_iterator<char>(t)), istreambuf_iterator<char>());
	findAndReplaceAll(str, ",", " ");
	findAndReplaceAll(str, "  ", " ");
	findAndReplaceAll(str, ".", ",");
	std::ofstream out(filePath.c_str());
	out << str;
	out.close();


	fclose(f_perf);

	cout << "Result file: " << filePath << endl;
	#if perfMeasure
		cout << "Result file: " << filePath_perf << endl;
	#endif

#endif

	// zobrazenie histogramov
	if (visual)
	{
		showHistogram(histogram, "Histogram");
		showHistogram(histM1, "histM1");
		showHistogram(histM2, "histM2");
		showHistogram(histM3, "histM3");
		showHistogram(histM4, "histM4");
		showHistogram(histM5, "histM5");
		showHistogram(histM6, "histM6");
		showHistogram(histM7, "histM7");
		showHistogram(histM8, "histM8");
		showHistogram(histP1, "histP1");
		showHistogram(histP1V, "histP1V");
		showHistogram(histP2, "histP2");
		showHistogram(histP3, "histP3");
		showHistogram(histP3V, "histP3V");
		showHistogram(histP4, "histP4");

		cout << endl << "Press any key to end." << endl;
		waitKey(0);
	}
	else if (showPicture)
		waitKey(0);

	#if showComputedPixImage
		if (!visual && !showPicture)
			waitKey(0);
	#endif

	return 0;
}


