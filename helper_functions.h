/*
 * ZPO projekt
 * Odhad histogramu obrazu
 * Autoři: David Novák, xnovak1m; Tomáš Hajdík, xhajdi01
 *
 */

#ifndef HELPER_FUNCTIONS_H
#define HELPER_FUNCTIONS_H

#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>
#include <chrono>
#include <vector>



#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

void mem_flush(const void *p, unsigned int allocation_size);
double compute_accur(std::vector<double>& h_ref_norm, std::vector<double>& h_estim);
void PrintMAX(const std::vector<double> RefHist);
double findMaxInHistogram(const std::vector<double> RefHist);
void printHistogram(const std::vector<double> histogram);
void showHistogram(const std::vector<double> histogram, std::string str);
void drawPixImage(const std::string& windowName, const cv::Mat& computedPixels);
void findAndReplaceAll(std::string & data, std::string toSearch, std::string replaceStr);

#endif

