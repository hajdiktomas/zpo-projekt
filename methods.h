/*
 * ZPO projekt
 * Odhad histogramu obrazu
 * Autoři: David Novák, xnovak1m; Tomáš Hajdík, xhajdi01
 *
 */

#ifndef METHODS_H
#define METHODS_H

#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>
#include <chrono>
#include <map>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>



int computeHistogram(const cv::Mat& image, std::vector<double>& histogram);


int M1_NthPixelHistogram(const cv::Mat& image, std::vector<double>& histogram, int pixelStep);
int M2_NthRowHistogram(const cv::Mat& image, std::vector<double>& histogram, int rowStep);
int M3_NthColHistogram(const cv::Mat& image, std::vector<double>& histogram, int colStep);
int M4_RndUnifHistogram(const cv::Mat& image, std::vector<double>& histogram, double amount);
int M5_RndUnifNoDuplicHistogram(const cv::Mat& image, std::vector<double>& histogram, double amount);
int M6_RndGausHistogram(const cv::Mat& image, std::vector<double>& histogram, double amount);
int M7_RndUnifClustHistogram(const cv::Mat& image, std::vector<double>& histogram, double amountOfPix, int cntOfClusters = 1000, double sigma = 50.0);
int M8_RandomNthHistogram(const cv::Mat& image, std::vector<double>& histogram, int pixelStepMin, int pixelStepMax);

int P1_CacheLinesHistogram(const cv::Mat& image, std::vector<double>& histogram, int stepSize, int pixelStep = 4);
int P1_CacheLinesHistogram(const cv::Mat& image, std::vector<double>& histogram, int stepSize, int pixelStepMin, int pixelStepMax);
int P2_VariableCacheLinesHistogram(const cv::Mat& image, std::vector<double>& histogram, int stepSizeMin, int stepSizeMax, int pixelStep = 4);
int P3_RandomCacheLinesHistogram(const cv::Mat& image, std::vector<double>& histogram, int stepSizeMin, int stepSizeMax, int pixelStep = 4);
int P3_RandomCacheLinesHistogram(const cv::Mat& image, std::vector<double>& histogram, int stepSizeMin, int stepSizeMax, int pixelStepMin, int pixelStepMax);
int P4_HeuristicCacheLinesHistogram(const cv::Mat& image, std::vector<double>& histogram, int stepSizeMin, int stepSizeMax, int pixelStep, int pixel_threshold, int step_threshold);

#endif

