/*
 * ZPO projekt
 * Odhad histogramu obrazu
 * Autoři: David Novák, xnovak1m; Tomáš Hajdík, xhajdi01
 *
 */

#include "helper_functions.h"
#include "settings.h"

using namespace std;
using namespace cv;

// funkce pro spláchnutí určité oblasti paměti z cache CPU
void mem_flush(const void *p, unsigned int allocation_size){
	const size_t cache_line = 64;
	const char *cp = (const char *)p;
	size_t i = 0;

	if (p == NULL || allocation_size <= 0)
		   return;

	for (i = 0; i < allocation_size; i += cache_line) {
		   asm volatile("clflush (%0)\n\t"
		                 :
		                 : "r"(&cp[i])
		                 : "memory");
	}

	asm volatile("sfence\n\t"
		           :
		           :
		           : "memory");
}

// funkcia spočíta a vypíše rozdiel histogramov
double compute_accur(vector<double>& h_ref_norm, vector<double>& h_estim){
	// string strArr[] = {"Correlation    ", "Chi-Square     ", "Intersection   ", "Bhattachar dist"};

	// normalizácie vypočítaného histogramu
	normalize(h_estim, h_estim, 1.0, 0.0, NORM_L1);

	// konverzia histogramu na typ float z dôvodu porovnania
	vector<float> h_r(h_ref_norm.begin(), h_ref_norm.end());
	vector<float> h_e(h_estim.begin(), h_estim.end());

	double acc = 0;
	for(std::vector<float>::size_type i = 0; i != h_r.size(); i++) {
    	acc += fabs(h_r[i] - h_e[i]);
    }
    return acc;
	// return compareHist( h_r, h_e, COMPARE_METHOD);
}

///////////////////////////////////////////////////////////////
//	Pomocné funkcie pre výpis hodnôt a vykreslenie histogramu
///////////////////////////////////////////////////////////////

void PrintMAX(const vector<double> RefHist){
	double max = 0.0;
	for (int i = 0; i < CNT_OF_HIST_BINS; i++){
		if(max < RefHist[i]) max = RefHist[i];
	}
	cout << "MAX " << max << endl;
}

// uloží maximum z referenčného histogramu pre použitie pri normalizácií
double findMaxInHistogram(const vector<double> RefHist){
	double max = 0.0;
	for (int i = 0; i < CNT_OF_HIST_BINS; i++){
		if(max < RefHist[i]) max = RefHist[i];
	}
	return max;
	// cout << "MAXrefHist " << MAXrefHist << endl;
}

// vypíše pole histogramu na stdout
// maximum zvýrazní červenou
void printHistogram(const vector<double> histogram){
	double max = findMaxInHistogram(histogram);

	for (int i = 0; i < CNT_OF_HIST_BINS; i++){
		if (!(i%COL_SCREEN_SIZE)) cout << endl;
		if (round(histogram[i]) == max) cout << "\033[1;31m" << "[" << i << "] " << histogram[i] << ",\t" << "\033[0m";
		else cout << "[" << i << "] " << histogram[i] << ",\t";
	}
	cout << endl;
}

// zobrazí normalizovaný histogram v rozmeroch 256*280
void showHistogram(const vector<double> histogram, string str){

	double maxHistVal = findMaxInHistogram(histogram);
	// cout << "MAX " << maxHistVal << endl;


	Mat histImage = Mat(280*2, 256*4, CV_8UC3, Scalar(0,128,0));

	// X*256/MAX
	vector<double> normalizedHist(CNT_OF_HIST_BINS, 0);
	for (int i = 0; i < CNT_OF_HIST_BINS; i++){
		normalizedHist[i] = histogram[i]*256*2/maxHistVal;
	}

	for (int i = 0; i < CNT_OF_HIST_BINS; i++){
		line( histImage, Point (i*4, 280*2-(int)normalizedHist[i]), Point (i*4, 280*2), Scalar( i, i, i ),  1, 8 );
		line( histImage, Point (i*4+1, 280*2-(int)normalizedHist[i]), Point (i*4+1, 280*2), Scalar( i, i, i ),  1, 8 );
		line( histImage, Point (i*4+2, 280*2-(int)normalizedHist[i]), Point (i*4+2, 280*2), Scalar( i, i, i ),  1, 8 );
		line( histImage, Point (i*4+3, 280*2-(int)normalizedHist[i]), Point (i*4+3, 280*2), Scalar( i, i, i ),  1, 8 );
	}
	namedWindow( str, WINDOW_AUTOSIZE );
	imshow( str, histImage );
}


// funkcia ktorá vykreslí obrázok s pixelmi použitými pre výpočet odhadu histohramu
void drawPixImage(const string& windowName, const Mat& computedPixels){
	namedWindow( windowName, WINDOW_AUTOSIZE );
	imshow( windowName, computedPixels );
	//imwrite( windowName + ".png", computedPixels );
}

// pomocna funkcia pre nahradenie retazca inym retazcom
//  prebrane z: https://thispointer.com/find-and-replace-all-occurrences-of-a-sub-string-in-c/
void findAndReplaceAll(std::string & data, std::string toSearch, std::string replaceStr)
{
	// Get the first occurrence
	size_t pos = data.find(toSearch);
 
	// Repeat till end is reached
	while( pos != std::string::npos)
	{
		// Replace this occurrence of Sub String
		data.replace(pos, toSearch.size(), replaceStr);
		// Get the next occurrence from the current position
		pos =data.find(toSearch, pos + replaceStr.size());
	}
}
