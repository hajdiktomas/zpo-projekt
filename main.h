/*
 * ZPO projekt
 * Odhad histogramu obrazu
 * Autoři: David Novák, xnovak1m; Tomáš Hajdík, xhajdi01
 *
 */

#ifndef MAIN_H
#define MAIN_H

#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>
#include <chrono>
#include <map>
#include <fstream>
#include <streambuf>
#include <getopt.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

typedef std::chrono::duration<double, std::milli> calc_duration;

void record_perf(std::string key, calc_duration dur);
void record_perf_min(std::string key, calc_duration dur);

#endif
