/*
 * ZPO projekt
 * Odhad histogramu obrazu
 * Autoři: David Novák, xnovak1m; Tomáš Hajdík, xhajdi01
 *
 */

// Poznámka: zakomentované define se nastavují při překladu v Makefile

#ifndef SETTINGS_H
#define SETTINGS_H

// 1=TRUE zobrazí pixely, ktoré boli použité k výpočtu
//#define showComputedPixImage 0

//#define perfMeasure 0
#define ITERATIONS 10

#define CACHE_LINE_SIZE 64

//#define showWindows 0

// počet prvkov histogramu
#define CNT_OF_HIST_BINS	256

// počet sloupců histogramu na šířku obrazovky
#define COL_SCREEN_SIZE 12

// default parameters
#define ARG_NTH 10
#define ARG_RAND 0.1
#define ARG_CLUSTERS 1000
#define ARG_SIGMA 50.0
#define ARG_CACHE 11
#define ARG_CACHE_MIN 1
#define ARG_CACHE_MAX 7
#define ARG_PIXEL 4
#define ARG_PIXEL_MIN 2
#define ARG_PIXEL_MAX 8
#define ARG_THR_PIXEL 20
#define ARG_THR_CACHE 7

#endif

