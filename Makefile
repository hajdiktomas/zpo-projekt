# /*
#  * ZPO projekt
#  * Odhad histogramu obrazu
#  * Autoři: David Novák, xnovak1m; Tomáš Hajdík, xhajdi01
#  *
#  */

NAME=hist_estimate
OBJFILES=helper_functions.o

CC=g++
CFLAGS=-O3 -I. -I/usr/local/include -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_imgcodecs

#settings - after changing, do "make clean" first
accurMeasure=1

SETTINGS= -DaccurMeasure=$(accurMeasure)

all: dep $(NAME)

window_$(NAME).o : $(NAME).cpp main.h settings.h helper_functions.h methods.h
	$(CC) $(CFLAGS) $(SETTINGS) -DshowComputedPixImage=1 -DshowWindows=1 -c $(NAME).cpp -o window_$(NAME).o

window_methods.o: methods.cpp main.h methods.h helper_functions.h
	$(CC) $(CFLAGS) -DshowComputedPixImage=1 -DshowWindows=1  -DaccurMeasure=$(accurMeasure) -c methods.cpp -o window_methods.o

$(NAME).o : $(NAME).cpp main.h settings.h helper_functions.h methods.h
	$(CC) $(CFLAGS) $(SETTINGS) -c $(NAME).cpp -o $(NAME).o

perf_$(NAME).o : $(NAME).cpp main.h settings.h helper_functions.h methods.h
	$(CC) $(CFLAGS) -DperfMeasure=1 -DaccurMeasure=$(accurMeasure) -c $(NAME).cpp -o perf_$(NAME).o

perf_methods.o: methods.cpp main.h methods.h helper_functions.h
	$(CC) $(CFLAGS) -DperfMeasure=1 -DaccurMeasure=$(accurMeasure) -c methods.cpp -o perf_methods.o

%.o : %.cpp
	$(CC) $(CFLAGS) $(SETTINGS) -c $<

$(NAME): $(OBJFILES) window_$(NAME).o $(NAME).o perf_$(NAME).o perf_methods.o methods.o window_methods.o
	mkdir -p csv
	$(CC) $(CFLAGS) $(SETTINGS) -DshowComputedPixImage=1 -DshowWindows=1 $(OBJFILES) window_methods.o window_$(NAME).o -o show_$(NAME)
	$(CC) $(CFLAGS) $(SETTINGS) $(OBJFILES) methods.o $(NAME).o -o $(NAME)
	$(CC) $(CFLAGS) -DperfMeasure=1 -DaccurMeasure=$(accurMeasure) $(OBJFILES) perf_methods.o perf_$(NAME).o -o perf_$(NAME)

dep:
	$(CC) -MM *.cpp >dep.list

zip:
	zip xnovak1m.zip *.cpp *.h Makefile *.csv readme.txt

-include dep.list

.PHONY: clean
clean:
	rm -f ~* *.o $(NAME) perf_$(NAME) show_$(NAME) dep.list xnovak1m.zip
