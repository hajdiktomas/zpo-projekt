/*
 * ZPO projekt
 * Odhad histogramu obrazu
 * Autoři: David Novák, xnovak1m; Tomáš Hajdík, xhajdi01
 *
 */

#include "main.h"
#include "methods.h"
#include "helper_functions.h"
#include "settings.h"

using namespace std;
using namespace std::chrono;
using namespace cv;

///////////////////////////////////////////////////////////////
//	Funkcia implementujúca výpočet histogramu
///////////////////////////////////////////////////////////////


// funkcia pre výpočet správneho histogramu zadaného obrázku
//		funkcia vráti počet spočítaných pixelov
int computeHistogram(const Mat& image, vector<double>& histogram){

#if perfMeasure
	mem_flush(image.ptr(0), image.elemSize() * image.total());
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
#endif

	for(int i = 0; i < (image.rows * image.cols); i++){	// riadky * stlpce
		// výpočet histogramu
		histogram[image.data[i]]++;
	}

#if perfMeasure
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	calc_duration dur = t2 - t1;

	record_perf_min("Z. Normální histogram", dur);
#endif

	return (image.rows * image.cols);
}



///////////////////////////////////////////////////////////////
//	Funkcie implementujúce metódy odhadu histogramu
///////////////////////////////////////////////////////////////


// funkcia pre odhad histogramu ktorá spočíta každý N-tý pixel
//		funkcia vráti počet spočítaných pixelov
int M1_NthPixelHistogram(const Mat& image, vector<double>& histogram, int pixelStep){

	std::fill(histogram.begin(), histogram.end(), 0.0);

	// počítadlo spočítaných pixelov
	int pixelCount = 0;

#if perfMeasure
	// spláchnout řádky cache, které obsahují data z načteného obrázku -> eliminace výkonového vlivu cache
	mem_flush(image.ptr(0), image.elemSize() * image.total());
	high_resolution_clock::time_point t1 = high_resolution_clock::now(); // zaznamenání času počátku výpočtu
#endif

	// kontrola maximálnej veľkosti kroku
	if (pixelStep > (image.rows * image.cols)) pixelStep = (image.rows * image.cols);

	// ------------------------------------------------------------
	#if showComputedPixImage
		Mat computedPixels = Mat::zeros(image.rows, image.cols, CV_8UC1); //<<<<<
	#endif
	// ------------------------------------------------------------

	for(int i = 0; i < (image.rows * image.cols); i+=pixelStep){	// riadky * stlpce

			// výpočet histogramu
			histogram[image.data[i]]++;

			#if not perfMeasure
				pixelCount++;
			#endif

			// ------------------------------------------------------------
			#if showComputedPixImage
				computedPixels.data[i] = 255; //<<<<<
			#endif
			// ------------------------------------------------------------
	}

#if perfMeasure
	high_resolution_clock::time_point t2 = high_resolution_clock::now(); // zaznamenání času konce výpočtu
	calc_duration dur = t2 - t1;

	record_perf_min("2. Každý n-tý pixel", dur);
#endif

	// ------------------------------------------------------------
	#if showComputedPixImage
		drawPixImage( "M1_NthPixelHistogram", computedPixels ); //<<<<<
	#endif
	// ------------------------------------------------------------

	return pixelCount;
}

// funkcia pre odhad histogramu ktorá spočíta každý N-tý riadok
//		funkcia vráti počet spočítaných pixelov
int M2_NthRowHistogram(const Mat& image, vector<double>& histogram, int rowStep){

	std::fill(histogram.begin(), histogram.end(), 0.0);

	// počítadlo spočítaných pixelov
	int pixelCount = 0;

#if perfMeasure
	mem_flush(image.ptr(0), image.elemSize() * image.total());
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
#endif

	// kontrola maximálnej veľkosti kroku
	if (rowStep > image.rows) rowStep = image.rows;

	// ------------------------------------------------------------
	#if showComputedPixImage
		Mat computedPixels = Mat::zeros(image.rows, image.cols, CV_8UC1); //<<<<<
	#endif
	// ------------------------------------------------------------

	for(int i = rowStep/2; i < image.rows; i+=rowStep){	// riadky - i
		for(int j = 0; j < image.cols; j++){			// stlpce - j

			// výpočet histogramu
			histogram[image.at<uchar>(i, j)]++;

			// aktualizuje počet spočítaných pixelov
			#if not perfMeasure
				pixelCount++;
			#endif

			// ------------------------------------------------------------
			#if showComputedPixImage
				computedPixels.at<uchar>(i, j) = 255; //<<<<<
			#endif
			// ------------------------------------------------------------
		}
	}

#if perfMeasure
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	calc_duration dur = t2 - t1;

	record_perf_min("3. Každý n-tý řádek", dur);
#endif

	// ------------------------------------------------------------
	#if showComputedPixImage
		drawPixImage( "M2_NthRowHistogram", computedPixels ); //<<<<<
	#endif
	// ------------------------------------------------------------

	return pixelCount;
}

// funkcia pre odhad histogramu ktorá spočíta každý N-tý stĺpec
//		funkcia vráti počet spočítaných pixelov
int M3_NthColHistogram(const Mat& image, vector<double>& histogram, int colStep){

	std::fill(histogram.begin(), histogram.end(), 0.0);

	// počítadlo spočítaných pixelov
	int pixelCount = 0;

#if perfMeasure
	mem_flush(image.ptr(0), image.elemSize() * image.total());
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
#endif

	// kontrola maximálnej veľkosti kroku
	if (colStep > image.rows) colStep = image.rows;

	// ------------------------------------------------------------
	#if showComputedPixImage
		Mat computedPixels = Mat::zeros(image.rows, image.cols, CV_8UC1); //<<<<<
	#endif
	// ------------------------------------------------------------

	for(int i = 0; i < image.rows; i++){	// riadky - i
		for(int j = colStep/2; j < image.cols; j+=colStep){			// stlpce - j

			// výpočet histogramu
			histogram[image.at<uchar>(i, j)]++;

			// aktualizuje počet spočítaných pixelov
			#if not perfMeasure
				pixelCount++;
			#endif

			// ------------------------------------------------------------
			#if showComputedPixImage
				computedPixels.at<uchar>(i, j) = 255; //<<<<<
			#endif
			// ------------------------------------------------------------
		}
	}

#if perfMeasure
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	calc_duration dur = t2 - t1;

	record_perf_min("4. Každý n-tý sloupec", dur);
#endif

	// ------------------------------------------------------------
	#if showComputedPixImage
		drawPixImage( "M3_NthColHistogram", computedPixels ); //<<<<<
	#endif
	// ------------------------------------------------------------

	return pixelCount;
}

// funkcia pre odhad histogramu ktorá spočíta zadané množstvo náhodných pixelov s rovnomerným rozložením
//		funkcia vráti počet spočítaných pixelov
//		amount: v rozsahu [0.0 .. 1.0]
int M4_RndUnifHistogram(const Mat& image, vector<double>& histogram, double amount){

	std::fill(histogram.begin(), histogram.end(), 0.0);

	// počítadlo spočítaných pixelov
	int pixelCount = 0;

	// inicializácia generátora náhodných čísel
	cv::RNG rng(cv::getTickCount());

#if perfMeasure
	mem_flush(image.ptr(0), image.elemSize() * image.total());
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
#endif

	// kontrola zadaného množstva náhodných pixelov
	if (amount > 1.0) amount = 1.0;
	if (amount < 0.0) amount = 0.0;

	// ------------------------------------------------------------
	#if showComputedPixImage
		Mat computedPixels = Mat::zeros(image.rows, image.cols, CV_8UC1); //<<<<<
	#endif
	// ------------------------------------------------------------

	for(int i = 0; i < (int) (image.rows * image.cols * amount); i++){	// riadky * stlpce

			// náhodne zvolí pozíciu rnd pixelu v obrázku
			int rnd = rng.uniform(0, image.rows * image.cols);

			// výpočet histogramu
			histogram[image.data[rnd]]++;

			#if not perfMeasure
				pixelCount++;
			#endif

			// ------------------------------------------------------------
			#if showComputedPixImage
				computedPixels.data[rnd] = 255; //<<<<<
			#endif
			// ------------------------------------------------------------
	}

#if perfMeasure
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	calc_duration dur = t2 - t1;

	record_perf("5. Náhodné pixely (uniform)", dur);
#endif

	// ------------------------------------------------------------
	#if showComputedPixImage
		drawPixImage( "M4_RndUnifHistogram", computedPixels ); //<<<<<
	#endif
	// ------------------------------------------------------------

	return pixelCount;
}

// funkcia pre odhad histogramu ktorá spočíta zadané množstvo náhodných
// pixelov s rovnomerným rozložením bez opakovania rovnakých pixelov
//		funkcia vráti počet spočítaných pixelov
//		amount: v rozsahu [0.0 .. 1.0]
int M5_RndUnifNoDuplicHistogram(const Mat& image, vector<double>& histogram, double amount){

	std::fill(histogram.begin(), histogram.end(), 0.0);

	// počítadlo spočítaných pixelov
	int pixelCount = 0;

	// inicializácia generátora náhodných čísel
	cv::RNG rng(cv::getTickCount());

#if perfMeasure
	mem_flush(image.ptr(0), image.elemSize() * image.total());
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
#endif

	// kontrola zadaného množstva náhodných pixelov
	if (amount > 1.0) amount = 1.0;
	if (amount < 0.0) amount = 0.0;

	// matica pre vyhľadanie či zvolený pixel už bol použitý
	Mat computedPixels = Mat::zeros(image.rows, image.cols, CV_8UC1); //<<<<<

	for(int i = 0; i < (int) (image.rows * image.cols * amount); i++){	// riadky * stlpce

			// náhodne zvolí pozíciu rnd pixelu v obrázku
			int rnd = rng.uniform(0, image.rows * image.cols);

			// kontroluje či už bol vygenerovaný pixel použitý pre odhad histogramu
			if (computedPixels.data[rnd] == 255){
				i--;
				continue;
			}

			// výpočet histogramu
			histogram[image.data[rnd]]++;

			#if not perfMeasure
				pixelCount++;
			#endif

			// vytvorenie značky pre použitý pixel
			computedPixels.data[rnd] = 255;
	}

#if perfMeasure
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	calc_duration dur = t2 - t1;

	record_perf("6. Náhodné pixely (uniform) s kontrolou duplicity", dur);
#endif

	// ------------------------------------------------------------
	#if showComputedPixImage
		drawPixImage( "M5_RndUnifNoDuplicHistogram", computedPixels ); //<<<<<
	#endif
	// ------------------------------------------------------------

	return pixelCount;
}

// funkcia pre odhad histogramu ktorá spočíta zadané množstvo náhodných pixelov s Gausovským rozložením
//		funkcia vráti počet spočítaných pixelov
//		amount: v rozsahu [0.0 .. 1.0]
int M6_RndGausHistogram(const Mat& image, vector<double>& histogram, double amount){

	std::fill(histogram.begin(), histogram.end(), 0.0);

	// počítadlo spočítaných pixelov
	int pixelCount = 0;

	// inicializácia generátora náhodných čísel
	cv::RNG rng(cv::getTickCount());

#if perfMeasure
	mem_flush(image.ptr(0), image.elemSize() * image.total());
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
#endif

	// sigma je polovica šírky obrázka
	double sigma = ((image.cols)/2);

	// kontrola zadaného množstva náhodných pixelov
	if (amount > 1.0) amount = 1.0;
	if (amount < 0.0) amount = 0.0;

	// ------------------------------------------------------------
	#if showComputedPixImage
		Mat computedPixels = Mat::zeros(image.rows, image.cols, CV_8UC1); //<<<<<
	#endif
	// ------------------------------------------------------------

	int image_size = image.rows * image.cols;

	for(int i = 0; i < (int) (image_size * amount); i++){	// riadky * stlpce

			// náhodne zvolí pozíciu rnd pixelu v obrázku
			int rnd = round(rng.gaussian(sigma)+(image_size/2));

			// kontrola či je náhodná pozícia pixelu v rozsahu obrázku
			if((rnd < 0)||(rnd >= image_size)){
				// ak nie opakuje generovanie
				i--;
				continue;
			}

			// výpočet histogramu
			histogram[image.data[rnd]]++;

			#if not perfMeasure
				pixelCount++;
			#endif

			// ------------------------------------------------------------
			#if showComputedPixImage
				computedPixels.data[rnd] = 255; //<<<<<
			#endif
			// ------------------------------------------------------------
	}

#if perfMeasure
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	calc_duration dur = t2 - t1;

	record_perf("7. Náhodné pixely (gauss)", dur);
#endif

	// ------------------------------------------------------------
	#if showComputedPixImage
		drawPixImage( "M6_RndGausHistogram", computedPixels ); //<<<<<
	#endif
	// ------------------------------------------------------------

	return pixelCount;
}


// funkcia pre odhad histogramu ktorá spočíta zadané množstvo náhodných pixelov v zhlukoch s Gausovským rozložením
//		funkcia vráti počet spočítaných pixelov
//		amount: v rozsahu [0.0 .. 1.0]
//		sigma: parameter Gausovho rozloženia pravdepodobnosti
int M7_RndUnifClustHistogram(const Mat& image, vector<double>& histogram, double amountOfPix, int cntOfClusters, double sigma){

	std::fill(histogram.begin(), histogram.end(), 0.0);

	// počítadlo spočítaných pixelov
	int pixelCount = 0;

	// inicializácia generátora náhodných čísel
	cv::RNG rng(cv::getTickCount());

#if perfMeasure
	mem_flush(image.ptr(0), image.elemSize() * image.total());
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
#endif

	// kontrola zadaného množstva náhodných pixelov
	if (amountOfPix > 1.0) amountOfPix = 1.0;
	if (amountOfPix < 0.0) amountOfPix = 0.0;

	// ------------------------------------------------------------
	#if showComputedPixImage
		Mat computedPixels = Mat::zeros(image.rows, image.cols, CV_8UC1); //<<<<<
	#endif
	// ------------------------------------------------------------

	for(int i = 0; i < cntOfClusters; i++){	// riadky * stlpce

			// náhodne zvolí pozíciu rnd_row pixelu v obrázku
			int rnd_row = rng.uniform(0, image.rows);
			// náhodne zvolí pozíciu rnd_col pixelu v obrázku
			int rnd_col = rng.uniform(0, image.cols);

			// generuje zhluk náhodných pixelov okolo aktuálnej pozície
			for (int j = 0; j < (int) ((image.rows * image.cols * amountOfPix)/cntOfClusters); j++){

				// náhodne zvolí pozíciu rnd_row pixelu v obrázku
				int rnd_pix_row = round(rng.gaussian(sigma)+(rnd_row));
				// náhodne zvolí pozíciu rnd_col pixelu v obrázku
				int rnd_pix_col = round(rng.gaussian(sigma)+(rnd_col));

				// kontrola či je náhodná pozícia pixelu v rozsahu obrázku
				if((rnd_pix_row < 0)||(rnd_pix_row >= image.rows)||(rnd_pix_col < 0)||(rnd_pix_col >= image.cols)){
					// ak nie opakuje generovanie
					j--;
					continue;
				}

				// výpočet histogramu
				histogram[image.at<uchar>(rnd_pix_row, rnd_pix_col)]++;

				#if not perfMeasure
					pixelCount++;
				#endif

				// ------------------------------------------------------------
				#if showComputedPixImage
					computedPixels.at<uchar>(rnd_pix_row, rnd_pix_col) = 255; //<<<<<
				#endif
				// ------------------------------------------------------------
			}
	}

#if perfMeasure
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	calc_duration dur = t2 - t1;

	record_perf("8. Náhodné pixely (gauss) ve shlucích", dur);
#endif

	// ------------------------------------------------------------
	#if showComputedPixImage
		drawPixImage( "M7_RndUnifClustHistogram", computedPixels ); //<<<<<
	#endif
	// ------------------------------------------------------------

	return pixelCount;
}

int M8_RandomNthHistogram(const Mat& image, vector<double>& histogram, int pixelStepMin, int pixelStepMax)
{
	std::fill(histogram.begin(), histogram.end(), 0.0);

	int pixelCount = 0;
	int pixelStep = pixelStepMin;
	cv::RNG rng(cv::getTickCount());

#if perfMeasure
	mem_flush(image.ptr(0), image.elemSize() * image.total());
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
#endif

	#if showComputedPixImage
		Mat computedPixels = Mat::zeros(image.rows, image.cols, CV_8UC1);
	#endif

	for(int i = 0; i < (image.rows * image.cols); i+=pixelStep)
	{
		// výpočet histogramu
		histogram[image.data[i]]++;

		pixelStep = rng.uniform(pixelStepMin, pixelStepMax);

		#if not perfMeasure
			pixelCount++;
		#endif

		#if showComputedPixImage
			computedPixels.data[i] = 255;
		#endif
	}

#if perfMeasure
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	calc_duration dur = t2 - t1;

	record_perf_min("9. N-tý pixel s náhodným přírůstkem", dur);
#endif

#if showComputedPixImage
	drawPixImage( "M8_RandomNthHistogram", computedPixels );
#endif

	return pixelCount;
}

/////////////////////////////////
// performance-friendly metody //
/////////////////////////////////
int P1_CacheLinesHistogram(const Mat& image, vector<double>& histogram, int stepSize, int pixelStep)
{
	std::fill(histogram.begin(), histogram.end(), 0.0);

	int pixelCount = 0;

#if perfMeasure
	mem_flush(image.ptr(0), image.elemSize() * image.total());
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
#endif

	#if showComputedPixImage
		Mat computedPixels = Mat::zeros(image.rows, image.cols, CV_8UC1);
	#endif

	for (int i = 0; i < (image.rows * image.cols); i+=stepSize*CACHE_LINE_SIZE)
	{
		for (int j = i; j < i+CACHE_LINE_SIZE; j+=pixelStep)
			histogram[image.data[j]]++;

		#if not perfMeasure
			pixelCount += 64/pixelStep;
		#endif

		#if showComputedPixImage
			for (int j = i; j < i+CACHE_LINE_SIZE; j+=pixelStep)
				computedPixels.data[j] = 255;
		#endif
	}

#if perfMeasure
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	calc_duration dur = t2 - t1;

	record_perf_min("A. Řádky cache", dur);
#endif

#if showComputedPixImage
	drawPixImage( "P1_CacheLinesHistogram", computedPixels );
#endif

	return pixelCount;
}

int P1_CacheLinesHistogram(const Mat& image, vector<double>& histogram, int stepSize, int pixelStepMin, int pixelStepMax)
{
	std::fill(histogram.begin(), histogram.end(), 0.0);

	int pixelCount = 0;
	int pixelStep = pixelStepMin;

#if perfMeasure
	mem_flush(image.ptr(0), image.elemSize() * image.total());
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
#endif

	#if showComputedPixImage
		Mat computedPixels = Mat::zeros(image.rows, image.cols, CV_8UC1);
	#endif

	for (int i = 0; i < (image.rows * image.cols); i+=stepSize*CACHE_LINE_SIZE)
	{
		for (int j = i; j < i+CACHE_LINE_SIZE; j+=pixelStep)
			histogram[image.data[j]]++;

		#if not perfMeasure
			pixelCount += 64/pixelStep;
		#endif

		#if showComputedPixImage
			for (int j = i; j < i+CACHE_LINE_SIZE; j+=pixelStep)
				computedPixels.data[j] = 255;
		#endif

		pixelStep++;
		if (pixelStep > pixelStepMax)
			pixelStep = pixelStepMin;
	}

#if perfMeasure
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	calc_duration dur = t2 - t1;

	record_perf_min("A2. Řádky cache s variabilním krokem pixelu", dur);
#endif

#if showComputedPixImage
	drawPixImage( "P1_CacheLinesHistogramVariablePixelStep", computedPixels );
#endif

	return pixelCount;
}

int P2_VariableCacheLinesHistogram(const Mat& image, vector<double>& histogram, int stepSizeMin, int stepSizeMax, int pixelStep)
{
	std::fill(histogram.begin(), histogram.end(), 0.0);

	int pixelCount = 0;

#if perfMeasure
	mem_flush(image.ptr(0), image.elemSize() * image.total());
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
#endif

	#if showComputedPixImage
		Mat computedPixels = Mat::zeros(image.rows, image.cols, CV_8UC1);
	#endif

	int stepSize = stepSizeMin;

	for (int i = 0; i < (image.rows * image.cols); i+=stepSize*CACHE_LINE_SIZE)
	{
		for (int j = i; j < i+CACHE_LINE_SIZE; j+=pixelStep)
			histogram[image.data[j]]++;

		stepSize++;
		if (stepSize > stepSizeMax)
			stepSize = stepSizeMin;

		#if not perfMeasure
			pixelCount += 64/pixelStep;
		#endif

		#if showComputedPixImage
			for (int j = i; j < i+CACHE_LINE_SIZE; j+=pixelStep)
				computedPixels.data[j] = 255;
		#endif
	}

#if perfMeasure
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	calc_duration dur = t2 - t1;

	record_perf_min("B. Řádky cache s variabilním krokem", dur);
#endif

#if showComputedPixImage
	drawPixImage( "P2_VariableCacheLinesHistogram", computedPixels );
#endif

	return pixelCount;
}

int P3_RandomCacheLinesHistogram(const Mat& image, vector<double>& histogram, int stepSizeMin, int stepSizeMax, int pixelStep)
{
	std::fill(histogram.begin(), histogram.end(), 0.0);

	int pixelCount = 0;
	int stepSize = stepSizeMin;
	cv::RNG rng(cv::getTickCount());

#if perfMeasure
	mem_flush(image.ptr(0), image.elemSize() * image.total());
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
#endif

	#if showComputedPixImage
		Mat computedPixels = Mat::zeros(image.rows, image.cols, CV_8UC1);
	#endif

	for (int i = 0; i < (image.rows * image.cols); i+=stepSize*CACHE_LINE_SIZE)
	{
		for (int j = i; j < i+CACHE_LINE_SIZE; j+=pixelStep)
			histogram[image.data[j]]++;

		#if not perfMeasure
			pixelCount += 64/pixelStep;
		#endif

		#if showComputedPixImage
			for (int j = i; j < i+CACHE_LINE_SIZE; j+=pixelStep)
				computedPixels.data[j] = 255;
		#endif

		stepSize = rng.uniform(stepSizeMin, stepSizeMax);
	}

#if perfMeasure
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	calc_duration dur = t2 - t1;

	record_perf("C. Řádky cache s náhodným krokem", dur);
#endif

#if showComputedPixImage
	drawPixImage( "P3_RandomCacheLinesHistogram", computedPixels );
#endif

	return pixelCount;
}

//P3_RandomCacheLinesRandomPixelStepHistogram
// also randomize pixelStep
int P3_RandomCacheLinesHistogram(const Mat& image, vector<double>& histogram, int stepSizeMin, int stepSizeMax, int pixelStepMin, int pixelStepMax)
{
	std::fill(histogram.begin(), histogram.end(), 0.0);

	int pixelCount = 0;
	int stepSize = stepSizeMin;
	int pixelStep = pixelStepMin;
	cv::RNG rng(cv::getTickCount());

#if perfMeasure
	mem_flush(image.ptr(0), image.elemSize() * image.total());
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
#endif

	#if showComputedPixImage
		Mat computedPixels = Mat::zeros(image.rows, image.cols, CV_8UC1);
	#endif

	for (int i = 0; i < (image.rows * image.cols); i+=stepSize*CACHE_LINE_SIZE)
	{
		for (int j = i; j < i+CACHE_LINE_SIZE; j+=pixelStep)
			histogram[image.data[j]]++;

		#if not perfMeasure
			pixelCount += 64/pixelStep;
		#endif

		#if showComputedPixImage
			for (int j = i; j < i+CACHE_LINE_SIZE; j+=pixelStep)
				computedPixels.data[j] = 255;
		#endif

		stepSize = rng.uniform(stepSizeMin, stepSizeMax);
		pixelStep = rng.uniform(pixelStepMin, pixelStepMax);
	}

#if perfMeasure
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	calc_duration dur = t2 - t1;

	record_perf("C2. Řádky cache s náhodným krokem a náhodným krokem pixelu", dur);
#endif

#if showComputedPixImage
	drawPixImage( "P3_RandomCacheLinesRandomPixelStepHistogram", computedPixels );
#endif

	return pixelCount;
}


int P4_HeuristicCacheLinesHistogram(const Mat& image, vector<double>& histogram, int stepSizeMin, int stepSizeMax, int pixelStep, int pixel_threshold, int step_threshold)
{
	std::fill(histogram.begin(), histogram.end(), 0.0);

	int pixelCount = 0;
	int pixelStepOld = pixelStep;
	int stepSize = stepSizeMin;

#if perfMeasure
	mem_flush(image.ptr(0), image.elemSize() * image.total());
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
#endif

	#if showComputedPixImage
		Mat computedPixels = Mat::zeros(image.rows, image.cols, CV_8UC1);
	#endif

	for (int i = 0; i < (image.rows * image.cols); i+=stepSize*CACHE_LINE_SIZE)
	{
		for (int j = i+pixelStep; j < i+CACHE_LINE_SIZE; j+=pixelStep)
		{
			if (abs(image.data[j-pixelStep] - image.data[j]) < pixel_threshold)
				pixelStep++;
			else
				pixelStep--;

			if (pixelStep < 2)
				pixelStep = 2;
			if (pixelStep > CACHE_LINE_SIZE/4)
				pixelStep = CACHE_LINE_SIZE/4;
		}

		if (pixelStep > pixelStepOld + step_threshold)
			stepSize++;
		else if (pixelStep < pixelStepOld + step_threshold)
			stepSize--;

		if (stepSize < stepSizeMin)
			stepSize = stepSizeMin;
		if (stepSize > stepSizeMax)
			stepSize = stepSizeMax;

		int inc = (stepSize - stepSizeMin)/2 + 1;

		for (int j = i; j < i+CACHE_LINE_SIZE; j+=pixelStep)
			histogram[image.data[j]] += inc;

		#if not perfMeasure
			pixelCount += 64/pixelStep;
		#endif

		#if showComputedPixImage
			for (int j = i; j < i+CACHE_LINE_SIZE; j+=pixelStep)
				computedPixels.data[j] = 255;
		#endif
	}

#if perfMeasure
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	calc_duration dur = t2 - t1;

	record_perf_min("D. Řádky cache s heuristickou analýzou pixelů", dur);
#endif

#if showComputedPixImage
	drawPixImage( "P4_HeuristicCacheLinesHistogram", computedPixels );
#endif

	return pixelCount;
}

